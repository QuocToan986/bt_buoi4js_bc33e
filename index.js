/******************bài 1*********************/
document.getElementById("btn-xuat-ra").onclick = function () {
  // input: number
  var num1El = document.getElementById("txt-num1").value * 1;
  var num2El = document.getElementById("txt-num2").value * 1;
  var num3El = document.getElementById("txt-num3").value * 1;
  // output: string
  var ketQua = "";
  // xử lý
  if (num1El > num2El && num1El > num3El && num2El > num3El) {
    ketQua += num3El + " " + num2El + " " + num1El;
  } else if (num1El > num2El && num1El > num3El && num3El > num2El) {
    ketQua += num2El + " " + num3El + " " + num1El;
  } else if (num2El > num1El && num2El > num3El && num1El > num3El) {
    ketQua += num3El + " " + num1El + " " + num2El;
  } else if (num2El > num1El && num2El > num3El && num3El > num1El) {
    ketQua += num1El + " " + num3El + " " + num2El;
  } else if (num3El > num1El && num3El > num2El && num2El > num1El) {
    ketQua += num1El + " " + num2El + " " + num3El;
  } else {
    ketQua += num2El + " " + num1El + " " + num3El;
  }
  document.getElementById("xuatRa").innerText = ketQua;
};

/******************bài 2*********************/
document.getElementById("btn-xuat").onclick = function () {
  //input: mối quan người thân
  var relationship = document.getElementById("txt-nguoi-than").value;
  var thongBao = "";

  //output: câu chào người thân
  var sayHello = "Xin chào";

  //proccess
  switch (relationship) {
    case "ba": {
      thongBao = sayHello + " " + relationship;
      break;
    }
    case "mẹ": {
      thongBao = sayHello + " " + relationship;
      break;
    }
    case "anh trai": {
      thongBao = sayHello + " " + relationship;
      break;
    }
    case "em gái": {
      thongBao = sayHello + " " + relationship;
      break;
    }
    default: {
      thongBao = "anh là ai? anh đi ra đi";
    }
  }
  document.getElementById("thongBao").innerHTML = thongBao;
};

/******************bài 3*********************/
document.getElementById("xuat").onclick = function () {
  //input: 3 số nguyên
  var soNguyen1 = document.getElementById("txt-number1").value * 1;
  var soNguyen2 = document.getElementById("txt-number2").value * 1;
  var soNguyen3 = document.getElementById("txt-number3").value * 1;
  //output: tổng số chẵn, tổng số lẽ
  var tongChan = 0;
  var tongLe = 0;
  // xử lý
  var count = 0;
  if (soNguyen1 % 2 === 0) {
    count++;
  }
  if (soNguyen2 % 2 === 0) {
    count++;
  }
  if (soNguyen3 % 2 === 0) {
    count++;
  }
  tongChan = count;
  tongLe = 3 - tongChan;
  document.getElementById(
    "showBai3"
  ).innerHTML = ` Tổng chẵn: ${tongChan} - Tổng lẻ: ${tongLe} `;
};

/******************bài 4*********************/
document.getElementById("xuat-tam-giac").onclick = function () {
  //input: nhập 3 cạnh tam giác
  var canhA = document.getElementById("txt-a").value * 1;
  var canhB = document.getElementById("txt-b").value * 1;
  var canhC = document.getElementById("txt-c").value * 1;
  //output: tên tam giác
  var nameTamGiac = "";
  if (canhA == 2 && canhB == 2 && canhC == 1) {
    nameTamGiac = "Tam giác cân";
    console.log("nameTamGiac: ", nameTamGiac);
  } else if (canhA == 3 && canhB == 3 && canhC == 3) {
    nameTamGiac = "Tam giác đều";
  } else if (canhA == 3 && canhB == 4 && canhC == 5) {
    nameTamGiac = "Tam giác vuông (định lý pytago)";
  } else {
    nameTamGiac = "Nhập cạnh không hợp lệ";
  }
  document.getElementById("ten-tam-giac").innerHTML = nameTamGiac;
};
